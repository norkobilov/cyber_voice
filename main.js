var isRequestInProgress = false;

function sendAudioPreprocessRequest() {
  if (isRequestInProgress) {
    // If a request is already in progress, do nothing
    return;
  }

  isRequestInProgress = true;

  var resultElement = document.getElementById('result');
  resultElement.style.display = 'block';
  resultElement.innerHTML = 'Please wait for the response...';

  var xhr = new XMLHttpRequest();
  xhr.open('GET', 'http://192.168.200.175:8000/audio_preprocess');

  xhr.onreadystatechange = function() {
    if (xhr.readyState === XMLHttpRequest.DONE) {
      if (xhr.status === 200) {
        var response = JSON.parse(xhr.responseText);
        var resultText = response.data;
        document.getElementById('result').textContent = resultText;
      } else {
        document.getElementById('result').textContent = 'Qayerdadur xato ketdi !';
      }

      isRequestInProgress = false;
    }
  };

  xhr.send();
}

function sendTrainModelRequest() {
  if (isRequestInProgress) {
    // If a request is already in progress, do nothing
    return;
  }

  isRequestInProgress = true;

  var trainingDataElement = document.getElementById('training-data');
  trainingDataElement.disabled = true;

  var resultElement = document.getElementById('result');
  resultElement.style.display = 'block';
  resultElement.innerHTML = '<p>Klaviaturaga tegmang ! Iltimos kuting ...</p>';

  var xhr = new XMLHttpRequest();
  xhr.open('GET', 'http://192.168.200.175:8000/train_model', true);

  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4 && xhr.status === 200) {
      trainingDataElement.disabled = false;

      var response = JSON.parse(xhr.responseText);
      var fileContents = response.data;

      resultElement.innerHTML = fileContents;

      // Update the last response time
      var lastResponseTime = new Date().toLocaleTimeString();
      localStorage.setItem('lastResponseTime', lastResponseTime);
      getCurrentTime();

      isRequestInProgress = false;
    }
  };

  xhr.send();
}

function sendPredictAudioRequest() {
  if (isRequestInProgress) {
    // If a request is already in progress, do nothing
    return;
  }

  var fileInput = document.getElementById('audio-upload-input');
  var file = fileInput.files[0];

  if (file) {
    // Check if the file format is .wav
    if (file.type !== 'audio/wav') {
      var status = document.getElementById('status');
      status.textContent = 'Faqat .wav formatidagi audio file ni tanlang !';
      setTimeout(function() {
        status.textContent = '';
      }, 3000);
      return;
    }

    isRequestInProgress = true;

    var resultElement = document.getElementById('result');
    resultElement.style.display = 'block';
    resultElement.innerHTML = '<p>Iltimos kutib turing ...</p>';

    var formData = new FormData();
    formData.append('file', file);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://192.168.200.175:8000/predict_audio');

    xhr.onreadystatechange = function() {
      if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
        var response = JSON.parse(xhr.responseText);
        var resultText = response.data;
        document.getElementById('result').textContent = resultText;
        isRequestInProgress = false;
      }
    };

    xhr.send(formData);
  } else {
    var status = document.getElementById('status');
    status.textContent = 'Iltimos, birinchi audio file ni tanlang !';
    setTimeout(function() {
      status.textContent = '';
    }, 2000);
  }
}

document.getElementById('reproduce').addEventListener('click', sendAudioPreprocessRequest);
document.getElementById('training-data').addEventListener('click', sendTrainModelRequest);
document.getElementById('result-btn').addEventListener('click', sendPredictAudioRequest);



$('.input-file input[type=file]').on('change', function(){
	let file = this.files[0];
	$(this).closest('.input-file').find('.input-file-text').html(file.name);
});



var lastResponseTime = localStorage.getItem('lastResponseTime');

function getCurrentTime() {
  var currentTime = new Date();
  var hours = currentTime.getHours();
  var minutes = currentTime.getMinutes();
  var days = currentTime.getDate();
  var month = currentTime.getMonth() + 1; // Months are zero-based
  var year = currentTime.getFullYear();

  if (minutes < 10) {
    minutes = '0' + minutes;
  }
  
  // Format the time as HH:MM
  var formattedTime = hours + ':' + minutes;
  
  // Format the date as DD/MM/YYYY
  var formattedDate = days + '/' + month + '/' + year;

  // Update the content of the 'current-time' paragraph
  document.getElementById('current-time').textContent = 'Oxirgi training vaqti: ' + formattedDate + ' ' + lastResponseTime;
}

// Call the getCurrentTime function to update the time initially
getCurrentTime();

// Call the getCurrentTime function every second to update the time dynamically
setInterval(getCurrentTime, 1000);


